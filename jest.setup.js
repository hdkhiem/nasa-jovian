import 'raf/polyfill';

import React from 'react';
import PropTypes from 'prop-types';
import { shallow, mount, render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import createStore from '~/store/createStore';
import { Provider } from 'react-redux';
import Immutable from 'immutable';

configure({ adapter: new Adapter() });

const initApp = (component) => {
  const store = createStore(Immutable.Map({}));

  return (
    <Provider store={store}>
      {component}
    </Provider>);
};

global.initApp = initApp;
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.__DEV__ = false;

global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

