import { createActions } from 'redux-actions';
import * as actionTypes from './BusinessActionTypes';

const businessActions = createActions({}, ...Object.values(actionTypes));

export default { ...businessActions };
