export const GET_NASA_COLLECTION = 'getNasaCollection';
export const GET_NASA_COLLECTION_COMPLETED = 'getNasaCollectionCompleted';
export const GET_NASA_COLLECTION_FAIL = 'getNasaCollectionFail';

export const CREATE_NASA_ITEM = 'createNasaItem';
export const CREATE_NASA_ITEM_COMPLETED = 'createNasaItemCompleted';
export const CREATE_NASA_ITEM_FAIL = 'createNasaItemFail';
export const CLEAR_CONFIRMATION_CREATED = 'clearConfirmationCreated';

export const PAGE_CHANGED = 'pageChanged';
