export const YOUR_API_KEY = `${process.env.YOUR_API_KEY}`;
export const YOUR_AUTH_DOMAIN = `${process.env.YOUR_AUTH_DOMAIN}`;
export const DATABASE_URL = `${process.env.DATABASE_URL}`;
export const PROJECT_ID = `${process.env.PROJECT_ID}`;
export const STORAGE_BUCKET = `${process.env.STORAGE_BUCKET}`;
export const FIREBASE_SENDER_ID = `${process.env.FIREBASE_SENDER_ID}`;
