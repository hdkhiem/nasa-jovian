import React from 'react';
import ReactDOM from 'react-dom';
import Immutable from 'immutable';
import { Provider } from 'react-redux';

import createStore from './store/createStore';
import Routes from './routes';

// Store Initialization
// ------------------------------------
const store = createStore(Immutable.Map({}));

// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root');

let render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <Routes />
    </Provider>
    ,
    MOUNT_NODE,
  );
};


// Development Tools
// ------------------------------------
if (__DEV__) {
  // eslint-disable-next-line import/no-extraneous-dependencies
  if (module.hot) {
    const renderApp = render;
    const renderError = (error) => {
      const RedBox = require('redbox-react').default;
      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE);
    };

    render = () => {
      try {
        renderApp();
      } catch (e) {
        renderError(e);
      }
    };
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render();
