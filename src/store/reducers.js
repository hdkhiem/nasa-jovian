import { combineReducers } from 'redux-immutable';
import { reducer as formReducer } from 'redux-form/immutable';
import toastReducer, { TOAST_STORE } from './toastReducer';
import nasaReducer, { NASA_STORE } from './nasaReducer';

const makeRootReducer = (asyncReducers) => {
  const formPlugin = {};
  const form = formReducer.plugin(formPlugin);

  const reducers = {
    ...asyncReducers,
    form,
  };
  reducers[TOAST_STORE] = toastReducer;
  reducers[NASA_STORE] = nasaReducer;

  return combineReducers(reducers);
};

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

  const _store = store;
  _store.asyncReducers[key] = reducer;
  _store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
