import { omit } from 'lodash';
import { ADD_TOAST, REMOVE_TOAST } from '~/actions/UIActionTypes';

export const TOAST_STORE = 'toasts';

export default function toasts(state = {}, action) {
  const { payload, type } = action;
  // payload = { type: "ERORR", messageKey: "canNotUpdate", messageParams: {key: value}};

  switch (type) {
  case ADD_TOAST: {
    // Checking duplicatie by messageKey
    if (!(payload && payload.messageKey) || state[payload.messageKey]) return state;

    const newState = { ...state };
    newState[payload.messageKey] = payload;
    return newState;
  }
  case REMOVE_TOAST: {
    return omit(state, payload);
  }
  default:
    return state;
  }
}
