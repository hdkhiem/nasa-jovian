import Immutable from 'immutable';
import {
  GET_NASA_COLLECTION_COMPLETED,
  GET_NASA_COLLECTION_FAIL,
  CREATE_NASA_ITEM_COMPLETED,
  CREATE_NASA_ITEM_FAIL,
  CLEAR_CONFIRMATION_CREATED,
  PAGE_CHANGED,
} from '~/actions/BusinessActionTypes';
import { keys, pick } from 'lodash';
import { PAGE_SIZE } from '~/config/config';


export const NASA_STORE = 'nasa';
export const NASA_COLLECTION = 'collection';
export const NASA_ACTIVE_COLLECTION = 'activeCollection';
export const NASA_TOTAL = 'total';
export const NASA_COMFIRMATION_CREATED = 'confirmationCreated';

const nasaReducer = (state = Immutable.Map({}), action) => {
  switch (action.type) {
  case GET_NASA_COLLECTION_COMPLETED:
    return state.withMutations((map) => {
      const activeKeys = keys(action.payload).slice(0, 9);

      map
        .set(NASA_COLLECTION, action.payload)
        .set(NASA_TOTAL, keys(action.payload).length)
        .set(NASA_ACTIVE_COLLECTION, pick(action.payload, activeKeys));
    });
  case GET_NASA_COLLECTION_FAIL:
    return state.set(NASA_COLLECTION, Immutable.Map({}));
  case CREATE_NASA_ITEM_COMPLETED:
    return state.set(NASA_COMFIRMATION_CREATED, true);
  case CREATE_NASA_ITEM_FAIL:
    return state.set(NASA_COMFIRMATION_CREATED, false);
  case CLEAR_CONFIRMATION_CREATED:
    return state.set(NASA_COMFIRMATION_CREATED, null);
  case PAGE_CHANGED: {
    // The pagination should be supported in SE side. This feature is implementing in FE side as a demo
    const page = action.payload;
    const collection = state.get(NASA_COLLECTION);
    const activeKeys = keys(collection).slice((page - 1) * PAGE_SIZE, page * PAGE_SIZE);
    return state.set(NASA_ACTIVE_COLLECTION, pick(collection, activeKeys));
  }
  default:
    return state;
  }
};

export default nasaReducer;
