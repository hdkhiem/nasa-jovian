import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import '~/styles/global.css';

import * as PATH from '~/config/routerConfig';
import project from '#/project.config';
import ToastContainer from '~/containers/Toast/ToastContainer';
import Home from '~/containers/Home/Home';
import NasaForm from '~/containers/NasaFormContainer/NasaFormContainer';

const Routes = () => (
  <BrowserRouter basename={project.publicPath} >
    <Fragment>
      <Switch>
        <Route exact path="/" name="Nasa Page" component={Home} />
        <Route exact path={PATH.NASA_FORM} name="Form Page" component={NasaForm} />
      </Switch>
      <ToastContainer />
    </Fragment>
  </BrowserRouter>
);

export default Routes;
