import pathToRegexp from 'path-to-regexp';

export const getQueryParam = (pattern, path, index) => {
  const re = pathToRegexp(pattern);
  const params = re.exec(path);
  const value = params[index];
  return value;
};

export const compileUrl = (pattern, params, host = '') => {
  let urlPattern = pattern;
  if (host) {
    urlPattern = pattern.replace(host, '');
  }
  const toPath = pathToRegexp.compile(urlPattern);
  return `${host}${toPath(params)}`;
};

export default getQueryParam;
