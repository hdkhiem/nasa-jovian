import { createSelector } from 'reselect';
import { TOAST_STORE } from '~/store/toastReducer';

const getToasts = state => state.get(TOAST_STORE);

const makeGetToasts = () => createSelector(
  getToasts,
  toastsState => toastsState,
);

export {
  getToasts,
  makeGetToasts,
};
