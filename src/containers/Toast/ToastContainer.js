import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer as ToastifyContainer, toast } from 'react-toastify';
import { createStructuredSelector } from 'reselect';
import { forEach } from 'lodash';
import { REMOVE_TOAST } from '~/actions/UIActionTypes';
import uIActions from '~/actions/UIActions';
import { AUTO_CLOSE_TOAST } from '~/config/config';
import { makeGetToasts } from './ToastSelector';

export const TYPE = toast.TYPE;
export const POSITION = toast.POSITION;

class ToastContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.onClose = this.onClose.bind(this);
    this.showToast = this.showToast.bind(this);

    this.activeToasts = {};
  }

  onClose({ messageKey }) {
    delete this.activeToasts[messageKey];
    this.props.removeToast(messageKey);
  }

  showToast(toasts) {
    forEach((toasts), (item) => {
      const options = {
        onClose: this.onClose,
        type: item.type || TYPE.INFO,
        position: item.postion || POSITION.TOP_CENTER,
      };

      if (!this.activeToasts[item.messageKey]) {
        const id = toast(
          <div messageKey={item.messageKey} key={item.messageKey}>{item.messageKey}</div>,
          options,
        );
        this.activeToasts[item.messageKey] = id;
      }
    });
  }

  render() {
    const { toasts } = this.props;
    this.showToast(toasts);

    return (
      <ToastifyContainer autoClose={AUTO_CLOSE_TOAST} position={POSITION.TOP_CENTER} />
    );
  }
}

ToastContainer.propTypes = {
  toasts: PropTypes.object.isRequired,
  removeToast: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeToast: uIActions[REMOVE_TOAST],
    },
    dispatch,
  );

const mapStateToProps = createStructuredSelector({
  toasts: makeGetToasts(),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToastContainer);
