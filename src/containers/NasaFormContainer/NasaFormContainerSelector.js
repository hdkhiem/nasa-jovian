import { createSelector } from 'reselect';
import { NASA_STORE, NASA_COMFIRMATION_CREATED } from '~/store/nasaReducer';

const getNasaState = state => state.get(NASA_STORE);

const makeConfirmationCreated = () => createSelector(
  getNasaState,
  searchState => searchState.get(NASA_COMFIRMATION_CREATED),
);

export {
  getNasaState,
  makeConfirmationCreated,
};
