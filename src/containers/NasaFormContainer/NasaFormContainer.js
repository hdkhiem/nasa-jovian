import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { isBoolean } from 'lodash';
import NasaForm from '~/components/NasaForm/NasaForm';
import businessActions from '~/actions/BusinessActions';
import { CREATE_NASA_ITEM, CLEAR_CONFIRMATION_CREATED } from '~/actions/BusinessActionTypes';
import { makeConfirmationCreated } from './NasaFormContainerSelector';

class NasaFormContainer extends Component {
  componentWillUnmount() {
    this.props.clearConfirmation();
  }

  render() {
    const { history, isCreated, createNasaItem } = this.props;

    if (isBoolean(isCreated) && isCreated) {
      history.push('/');
      return null;
    }

    return (
      <NasaForm
        createNasaItem={createNasaItem}
      />
    );
  }
}

NasaFormContainer.propTypes = {
  isCreated: PropTypes.bool,
  history: PropTypes.object.isRequired,
  createNasaItem: PropTypes.func.isRequired,
  clearConfirmation: PropTypes.func.isRequired,
};

NasaFormContainer.defaultProps = {
  isCreated: false,
};

const mapStateToProps = createStructuredSelector({
  isCreated: makeConfirmationCreated(),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createNasaItem: businessActions[CREATE_NASA_ITEM],
      clearConfirmation: businessActions[CLEAR_CONFIRMATION_CREATED],
      dispatch,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(NasaFormContainer);
