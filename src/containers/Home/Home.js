import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Button } from 'reactstrap';
import businessActions from '~/actions/BusinessActions';
import { GET_NASA_COLLECTION, PAGE_CHANGED } from '~/actions/BusinessActionTypes';
import NasaCollection from '~/components/NasaCollection/NasaCollection';
import { NASA_FORM } from '~/config/routerConfig';
import { makeCollection, makeTotalNumber } from './HomeSelector';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
    };
    this.goToNasaForm = this.goToNasaForm.bind(this);
    this.pageChangedHandler = this.pageChangedHandler.bind(this);
  }

  componentDidMount() {
    this.props.getCollection();
  }

  goToNasaForm() {
    this.props.history.push(NASA_FORM);
  }

  pageChangedHandler(page) {
    this.props.pageChanged(page);
    this.setState({
      currentPage: page,
    });
  }

  render() {
    const { collection, total } = this.props;
    const { currentPage } = this.state;

    return (
      <div className="container">
        <h1 className="text-center">NASA</h1>
        <Button color="primary" size="lg" block onClick={this.goToNasaForm} >Add item with all field inputs</Button>
        <NasaCollection
          collection={collection}
          currentPage={currentPage}
          total={total}
          pageChangedHandler={this.pageChangedHandler}
        />
      </div>
    );
  }
}

Home.propTypes = {
  getCollection: PropTypes.func.isRequired,
  collection: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  pageChanged: PropTypes.func.isRequired,
  total: PropTypes.number,
};

Home.defaultProps = {
  collection: {},
  total: 10,
};

const mapStateToProps = createStructuredSelector({
  collection: makeCollection(),
  total: makeTotalNumber(),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getCollection: businessActions[GET_NASA_COLLECTION],
      pageChanged: businessActions[PAGE_CHANGED],
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
