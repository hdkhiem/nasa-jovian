import { createSelector } from 'reselect';
import { NASA_STORE, NASA_ACTIVE_COLLECTION, NASA_TOTAL } from '~/store/nasaReducer';

const getStore = state => state.get(NASA_STORE);

const makeCollection = () => createSelector(
  getStore,
  store => store.get(NASA_ACTIVE_COLLECTION),
);

const makeTotalNumber = () => createSelector(
  getStore,
  store => store.get(NASA_TOTAL),
);

export {
  getStore,
  makeCollection,
  makeTotalNumber,
};
