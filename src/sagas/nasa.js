import { put, call, takeLatest } from 'redux-saga/effects';
import {
  GET_NASA_COLLECTION,
  GET_NASA_COLLECTION_COMPLETED,
  GET_NASA_COLLECTION_FAIL,
  CREATE_NASA_ITEM,
  CREATE_NASA_ITEM_COMPLETED,
  CREATE_NASA_ITEM_FAIL,
} from '~/actions/BusinessActionTypes';
import { ADD_TOAST } from '~/actions/UIActionTypes';
import { getNasaCollectionApi, createNasaItemApi, uploadFileApi } from '~/services/apis/nasa.js';
import { TYPE } from '~/containers/Toast/ToastContainer';

function* getNasaCollection() {
  try {
    const response = yield call(getNasaCollectionApi);
    yield put({
      type: GET_NASA_COLLECTION_COMPLETED,
      payload: response.val(),
    });
  } catch (e) {
    // Delete collection in store
    yield put({
      type: GET_NASA_COLLECTION_FAIL,
    });
    // Show a error toast message
    yield put({
      type: ADD_TOAST,
      payload: {
        messageKey: (e && e.message) || 'error',
        type: TYPE.ERROR,
      },
    });
  }
}

function* createNasaItem(action) {
  try {
    const { media, nasa_id, title, description, date_created, media_type } = action.payload;
    const href = yield call(uploadFileApi, { media: media[0], nasa_id });
    yield call(createNasaItemApi, { href, ...action.payload });

    // Show a error toast message
    yield put({
      type: ADD_TOAST,
      payload: {
        messageKey: 'The form is successfully submitted',
      },
    });

    yield put({
      type: CREATE_NASA_ITEM_COMPLETED,
    });
  } catch (e) {
    // Delete collection in store
    yield put({
      type: CREATE_NASA_ITEM_FAIL,
    });
    // Show a error toast message
    yield put({
      type: ADD_TOAST,
      payload: {
        messageKey: e || 'error',
        type: TYPE.ERROR,
      },
    });
  }
}

export function* watchCreateNasaItem() {
  yield takeLatest(CREATE_NASA_ITEM, createNasaItem);
}

export function* watchGetNasaCollection() {
  yield takeLatest(GET_NASA_COLLECTION, getNasaCollection);
}

export default watchGetNasaCollection;
