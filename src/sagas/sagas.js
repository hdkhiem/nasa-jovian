import { fork } from 'redux-saga/effects';
import { watchGetNasaCollection, watchCreateNasaItem } from './nasa';

export default function* rootSaga() {
  yield fork(watchGetNasaCollection);
  yield fork(watchCreateNasaItem);
}
