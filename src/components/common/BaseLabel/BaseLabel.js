import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './BaseLabel.css';

const BaseLabel = ({ htmlFor, label, isRequired }) => (
  <label htmlFor={htmlFor} className={classNames(styles.label, isRequired === true ? styles.required : '')}>
    { label }
  </label>
);

BaseLabel.propTypes = {
  htmlFor: PropTypes.string,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
};

BaseLabel.defaultProps = {
  htmlFor: null,
  label: null,
  isRequired: false,
};
export default BaseLabel;
