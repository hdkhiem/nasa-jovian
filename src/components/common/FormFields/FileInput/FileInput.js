import React from 'react';
import PropTypes from 'prop-types';
import styles from './FileInput.css';

const FileInput = ({
  input: { value: omitValue, ...inputProps }, meta: omitMeta, accept, ...props
}) => (
  <input
    type="file"
    {...inputProps}
    {...props}
    className={omitMeta.touched && omitMeta.error ? styles.error : ''}
    accept={accept}
  />
);

FileInput.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  accept: PropTypes.string,
};

FileInput.defaultProps = {
  input: null,
  meta: null,
  accept: "image/jpg,audio/wav,video/mp4",
};

export default FileInput;
