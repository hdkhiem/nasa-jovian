import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withLabelAndMessage from '~/hocs/withLabelAndMessage/withLabelAndMessage';
import styles from './TextFormField.css';

const TextFormField = (props) => {
  const {
    id, placeholder, text, meta, input, type, className, errorClass,
  } = props;
  const errorClassname = meta.touched && meta.error ? (errorClass || styles.error) : '';
  delete input.value; // Using defaultValue

  return (
    <input
      className={classNames('form-control', styles.textInput, errorClassname, className)}
      id={id}
      type={type}
      defaultValue={text}
      {...input}
      placeholder={placeholder}
    />
  );
};

TextFormField.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  errorClass: PropTypes.string,
  placeholder: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
};

TextFormField.defaultProps = {
  id: null,
  text: '',
  placeholder: '',
  className: '',
  errorClass: '',
  type: 'text',
  meta: {},
  input: {},
};

export default withLabelAndMessage(TextFormField);
