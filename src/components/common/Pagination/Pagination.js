import React, { PureComponent } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import PaginationComponent from 'react-js-pagination';
import styles from './Pagination.css';

class Pagination extends PureComponent {
  onChange = (page) => {
    this.props.onClickHandler(page);
  }

  render() {
    const {
      pageSize, currentPage, total, className,
    } = this.props;


    if (total < pageSize) {
      return (<div />);
    }

    return (
      <div className={classNames(styles.container, className)}>
        <PaginationComponent
          activePage={currentPage}
          itemsCountPerPage={pageSize}
          totalItemsCount={total}
          onChange={this.onChange}
        />
      </div>
    );
  }
}

Pagination.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  className: PropTypes.string,
};

Pagination.defaultProps = {
  onClickHandler: null,
  pageSize: 0,
  currentPage: 1,
  total: 0,
  className: '',
};

export default Pagination;
