import React from 'react';
import PropTypes from 'prop-types';
import { getMediaSource } from '~/services/media';

export const NasaMedia = ({
  media_type, nasa_id, href,
}) => {
  let media = '';
  let source = '';
  if (href.indexOf('firebasestorage') === -1 ) {
    source = getMediaSource(media_type, nasa_id) || '';
  } else {
    source = href;
  }
  
  switch (media_type) {
  case 'video':
    media = (
      <video width="320" height="240" controls>
        <source src={source} type="video/mp4" />
      Your browser does not support the video tag.
      </video>);
    break;
  case 'audio':
    media = (
      <audio controls>
        <source src={source} type="audio/wav" />
      Your browser does not support the audio tag.
      </audio>);
    break;
  case 'image':
    media = (<img width="320" src={source} />);
    break;
  default:
    link = (<img src={source} />);
    break;
  }

  return media;
};

NasaMedia.propTypes = {
  media_type: PropTypes.string,
  nasa_id: PropTypes.string,
};

NasaMedia.defaultProps = {
  media_type: '',
  nasa_id: '',
};

export default NasaMedia;
