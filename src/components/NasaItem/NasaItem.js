import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Button } from 'reactstrap';
import NasaMedia from '~/components/NasaMedia/NasaMedia';

export const NasaItem = ({
  title, description, date_created, href, media_type, nasa_id,
}) => (
  <tr>
    <td>
      {title}
    </td>
    <td>
      {description}
    </td>
    <td>
      {moment(date_created).format('MMM D, YYYY')}
    </td>
    <td>
      <NasaMedia
        media_type={media_type}
        nasa_id={nasa_id}
        href={href}
      />
    </td>
    <td>
      <a href={href} target="_blank">
        DOWNLOAD
      </a>
    </td>
  </tr>
);

NasaItem.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  date_created: PropTypes.string,
  href: PropTypes.string,
};

NasaItem.defaultProps = {
  title: '',
  description: '',
  date_created: '',
  href: '',
};

export default NasaItem;
