import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'reactstrap';
import { forIn } from 'lodash';
import NasaItem from '~/components/NasaItem/NasaItem';
import Pagination from '~/components/common/Pagination/Pagination';
import { PAGE_SIZE } from '~/config/config';

export class NasaCollection extends PureComponent {
  render() {
    const {
      collection, pageChangedHandler, currentPage, total,
    } = this.props;

    const collectionItems = [];
    forIn(collection, (item) => {
      if (item && item.data) {
        collectionItems.push(
          <NasaItem
            key={item.data[0].nasa_id}
            {...item.data[0]}
            href={item.href}
          />,
        );
      }
    });

    return (
      <div className="mt-3">
        <Table bordered className="mtb-3">
          <thead>
            <tr>
              <th style={{ width: '15%' }}>Title</th>
              <th style={{ width: '45%' }}>Description</th>
              <th style={{ width: '15%' }}>Date created</th>
              <th style={{ width: '15%' }}>Preview</th>
              <th style={{ width: '5%' }}>Download</th>
            </tr>
          </thead>
          <tbody>
            {collectionItems}
          </tbody>
        </Table>
        <Pagination
          onClickHandler={pageChangedHandler}
          pageSize={PAGE_SIZE}
          total={total}
          currentPage={currentPage}
          className="w-100"
        />
      </div>
    );
  }
}

NasaCollection.propTypes = {
  collection: PropTypes.object.isRequired,
  pageChangedHandler: PropTypes.func.isRequired,
  currentPage: PropTypes.number,
  total: PropTypes.number,
};

NasaCollection.defaultProps = {
  currentPage: 1,
  total: 10,
};

export default NasaCollection;
