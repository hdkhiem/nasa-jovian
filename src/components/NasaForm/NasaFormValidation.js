const validate = (values) => {
  // values is an Immutable.Map here!
  const errors = {};

  if (!values.get('title')) {
    errors.title = 'Required';
  }

  if (!values.get('media')) {
    errors.media = 'Required';
  }

  return errors;
};

export default validate;
