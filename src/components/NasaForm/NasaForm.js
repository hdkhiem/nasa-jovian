import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form/immutable';
import uuid from 'uuid/v4';
import TextFormField from '~/components/common/FormFields/TextFormField/TextFormField';
import FileInput from '~/components/common/FormFields/FileInput/FileInput';
import validate from './NasaFormValidation.js';
import styles from './NasaForm.css';

class NasaForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      waiting: false,
    };

    this.submitForm = this.submitForm.bind(this);
  }

  submitForm = (values) => {
    // Block submit btn
    this.setState({
      waiting: true,
    });
    // Standardize data
    const submitData = values.toJS();
    submitData.date_created = (new Date()).toISOString();
    const fileType = submitData.media && submitData.media[0].type;
    submitData.media_type = (fileType && fileType.split('/')[0]) || 'image';
    submitData.nasa_id = uuid();
    submitData.description = submitData.description || '';
    this.props.createNasaItem(submitData);
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="container">
        <h1 className="text-center">Add item with all field inputs</h1>
        <form onSubmit={handleSubmit(this.submitForm)}>
          <Field
            name="title"
            component={TextFormField}
            placeholder="Title"
            errorClass={styles.error}
            className={styles.input}
            showLabel={false}
            isRequired
          />
          <Field
            name="description"
            component={TextFormField}
            placeholder="Description"
            errorClass={styles.error}
            className={styles.input}
            showLabel={false}
          />
          <Field
            type="file"
            name="media"
            component={FileInput}
          />
          <div className="text-center">
            <input
              className={`btn ${styles.submitBtn}`}
              type="submit"
              disabled={this.state.waiting}
              value={this.state.waiting ? 'Please wait' : 'Submit'}
            />
          </div>
        </form>
      </div>
    );
  }
}

NasaForm.propTypes = {
  createNasaItem: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

const NasaFormRedux = reduxForm({
  form: 'NasaForm',
  validate,
})(NasaForm);

export default NasaFormRedux;
