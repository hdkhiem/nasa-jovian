import firebase from '~/services/firebase';
import axios from 'axios';
import { GET_NASA_COLLECTION_URL } from '~/config/uri';
import { PAGE_SIZE } from '~/config/config';

export function getNasaCollectionApi() {
  return firebase.database().ref('/collection')
    .once('value');
}

export function searchNasaMediaApi({ q = '', page = 1 }) {
  return axios.get(GET_NASA_COLLECTION_URL, {
    q,
    page,
    media: 'image,video,audio',
    yearStart: '1920',
    yearEnd: '2018',
  });
}

export function createNasaItemApi({
  title = '', description = '', date_created = '', media_type = '', nasa_id = '', href = '',
}) {
  return firebase.database().ref().child('collection').push({
    data: [{
      title, description, date_created, media_type, nasa_id,
    }],
    href,
  });
}

export function uploadFileApi({ media, nasa_id = '' }) {
  const promise = new Promise(((resolve, reject) => {
    const storageRef = firebase.storage().ref();
    const mountainsRef = storageRef.child(nasa_id);
    mountainsRef.put(media).then(() => {
      mountainsRef.getDownloadURL().then((url) => {
        resolve(url);
      });
    }).catch(() => {
      reject(new Error('Can not upload file'));
    });
  }));

  return promise;
}
