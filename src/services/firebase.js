import {
  YOUR_API_KEY,
  YOUR_AUTH_DOMAIN,
  DATABASE_URL,
  PROJECT_ID,
  STORAGE_BUCKET,
  FIREBASE_SENDER_ID,
} from '~/const/Firebase';
import firebase from 'firebase';

// Initalize and export Firebase.
const config = {
  apiKey: YOUR_API_KEY,
  authDomain: YOUR_AUTH_DOMAIN,
  databaseURL: DATABASE_URL,
  projectId: PROJECT_ID,
  storageBucket: STORAGE_BUCKET,
  messagingSenderId: FIREBASE_SENDER_ID,
};

export default firebase.initializeApp(config);
