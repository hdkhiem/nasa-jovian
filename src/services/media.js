const AUDIO = 'http://images-assets.nasa.gov/audio/{id}/{id}~orig.wav';
const VIDEO = 'http://images-assets.nasa.gov/video/{id}/{id}~orig.mp4';
const IMAGE = 'http://images-assets.nasa.gov/image/{id}/{id}~orig.jpg';

export function getMediaSource(type, id) {
  let link = '';
  switch (type) {
  case 'video':
    link = VIDEO;
    break;
  case 'audio':
    link = AUDIO;
    break;
  case 'image':
    link = IMAGE;
    break;
  default:
    link = IMAGE;
    break;
  }

  return link.replace(/{id}/g, id);
}

export default getMediaSource;
