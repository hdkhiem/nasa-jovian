module.exports = {
    plugins: [
        require('postcss-import')({path: ["src/styles"]}),
        require('postcss-advanced-variables'),
        require('postcss-image-set-polyfill'),
        require('postcss-initial'),
        require('postcss-cssnext'),
        require('postcss-utilities'),
        require('postcss-nested'),
        require('lost'),
    ]
};
