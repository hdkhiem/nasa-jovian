// Production Optimizations
// ------------------------------------
const webpack = require('webpack');
const project = require('../../project.config');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
        }),
        new UglifyJsPlugin({
            sourceMap: project.sourcemaps ? true : false,
            uglifyOptions: {
                output: {
                    comments: false,
                    beautify: false,
                },
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true,
            },
        }),
    ],
};

module.exports = config;