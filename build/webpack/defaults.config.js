const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const project = require('../../project.config');
const merge = require('webpack-merge');

const inProject = path.resolve.bind(path, project.basePath);
const inProjectSrc = (file) => inProject(project.srcDir, file);

const __DEV__ = project.env === 'development';
const __TEST__ = project.env === 'test';
const __PROD__ = project.env === 'production';

let config = {
    entry: {
        normalize: [
            inProjectSrc('normalize'),
        ],
        main: [
            inProjectSrc(project.main),
        ],
    },
    devtool: project.sourcemaps ? 'source-map' : false,
    output: {
        path: inProject(project.outDir),
        filename: '[name].[chunkhash].js',
        publicPath: project.publicPath,
    },
    resolve: {
        modules: [
            inProject(project.srcDir),
            'node_modules',
        ],
        extensions: ['*', '.js', '.jsx', '.json'],
    },
    externals: project.externals,
    module: {
        rules: [],
    },
    plugins: [
        new webpack.DefinePlugin(Object.assign({
            'process.env': { NODE_ENV: JSON.stringify(project.env) },
            __DEV__,
            __TEST__,
            __PROD__,
        }, project.globals))
    ],
};

// Bundle Splitting
// ------------------------------------
if (!__TEST__) {
    const bundles = ['normalize', 'manifest'];
    
    if (project.vendors && project.vendors.length) {
        bundles.unshift('vendor');
        config.entry.vendor = project.vendors;
    }
    config.plugins.push(new webpack.optimize.CommonsChunkPlugin({ names: bundles }));
}

const font = require('./font.config');
const html = require('./html.config');
const image = require('./image.config');
const javascript = require('./javascript.config');
const style = require('./style.config');

config = merge.smart(config, font);
config = merge.smart(config, html);
config = merge.smart(config, image);
config = merge.smart(config, javascript);
config = merge.smart(config, style);

module.exports = config;
