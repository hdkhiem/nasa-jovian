const path = require('path');
const project = require('../../project.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const inProject = path.resolve.bind(path, project.basePath);
const inProjectSrc = (file) => inProject(project.srcDir, file);

const __DEV__ = project.env === 'development';

const extractStyles = new ExtractTextPlugin({
    filename: 'styles/[name].[contenthash].css',
    allChunks: true,
    disable: __DEV__,
});

const globalExtractStyles = new ExtractTextPlugin({
    filename: 'styles/global.[contenthash].css',
    allChunks: true,
    disable: __DEV__,
});

const localIdentName = '[name]__[local]';

const config = {
    module: {
        rules: [
            {
                test: /\.css$/,
                include: [/node_modules\/antd/, inProjectSrc('styles/global.css')],
                loader: globalExtractStyles.extract({
                    fallback: 'style-loader',
                    use: [
                        { loader: 'css-loader' },
                        { loader: 'postcss-loader',
                        options: {
                            config: {
                                path: './build/postcss.config.js'
                            }
                        }}
                    ]
                }),
            },
            {
                test: /\.css$/,
                exclude: [/node_modules/, inProjectSrc('styles/global.css')],
                loader: extractStyles.extract({
                    fallback: 'style-loader',
                    use: [
                        { loader: 'css-loader', query: { modules: true, sourceMaps: true, importLoaders: 1 , localIdentName: localIdentName } },
                        { loader: 'postcss-loader',
                        options: {
                            config: {
                                path: './build/postcss.config.js'
                            }
                        }}
                    ]
                })
            }
        ]
    },
    plugins: [
        extractStyles,
        globalExtractStyles,
    ],
};

module.exports = config;