// Development Tools
// ------------------------------------
const webpack = require('webpack');
const project = require('../../project.config');

const config = {
    output: {
        filename: '[name].js',
    },
    entry: {
        main: [
            `webpack-hot-middleware/client.js?path=${project.publicPath}__webpack_hmr`,
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ],
};

module.exports = config;
