const config = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules(?!\/antd)/,
                use: [{
                    loader: 'babel-loader',
                    query: {
                        cacheDirectory: true,
                        plugins: [
                            ['import', {'libraryName': 'antd', 'style': 'css'}],
                            'transform-decorators-legacy',
                            'babel-plugin-transform-class-properties',
                            'babel-plugin-syntax-dynamic-import',
                            [
                                "babel-plugin-root-import",
                                [{
                                    "rootPathPrefix": "~", // `~` is the default so you can remove this if you want
                                    "rootPathSuffix": "src"
                                },
                                {
                                    "rootPathPrefix": "#",
                                    "rootPathSuffix": "." // since we suport relative paths you can also go into a parent directory
                                }]
                            ],
                            [
                                'babel-plugin-transform-runtime',
                                {
                                    helpers: true,
                                    polyfill: false, // we polyfill needed features in src/normalize.js
                                    regenerator: true,
                                },
                            ],
                            [
                                'babel-plugin-transform-object-rest-spread',
                                {
                                    useBuiltIns: true // we polyfill Object.assign in src/normalize.js
                                },
                            ],
                        ],
                        presets: [
                            'babel-preset-react',
                            ['babel-preset-env', {
                                targets: {
                                    ie9: true,
                                    uglify: true,
                                    modules: false,
                                },
                            }],
                        ]
                    },
                }],
            },
        ]
    }
};

module.exports = config;