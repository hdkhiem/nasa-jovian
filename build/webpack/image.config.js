const config = {
    module: {
        rules: [
            {
                test    : /\.(png|jpg|gif)$/,
                loader  : 'url-loader',
                options : {
                    limit : 8192,
                },
            },
            {
                test    : /\.svg$/,
                loader  : 'raw-loader',
            }
        ],
    }
};

module.exports = config;