const path = require('path');
const project = require('../../project.config');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const inProject = path.resolve.bind(path, project.basePath);
const inProjectSrc = (file) => inProject(project.srcDir, file);

const config = {
    plugins: [
        new HtmlWebpackPlugin({
            template: inProjectSrc('index.html'),
            inject: true,
            minify: {
                collapseWhitespace: true,
            },
        })
    ],
};

module.exports = config;