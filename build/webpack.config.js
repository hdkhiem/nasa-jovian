const merge = require('webpack-merge');
const defaults = require('./webpack/defaults.config.js');
const config = require('./webpack/' + (process.env.NODE_ENV || 'development') + '.config.js');
const configMerged = merge.smart(defaults, config);
module.exports = configMerged;