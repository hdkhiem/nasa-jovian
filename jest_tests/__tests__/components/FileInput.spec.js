import React from 'react';
import FileInput from '~/components/common/FormFields/FileInput/FileInput';
import renderer from 'react-test-renderer';

describe('(Component) FileInput', () => {
  let props;
  let component;

  beforeEach(() => {
    props = {
      accept: 'image/jpg,audio/wav,video/mp4',
      input: {},
      meta: {},
    };

    component = mount(<FileInput {...props} />);
  });

  it('should renders correctly', () => {
    const tree = renderer.create(component).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
