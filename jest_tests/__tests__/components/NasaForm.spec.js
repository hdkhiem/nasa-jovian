import React from 'react';
import NasaForm from '~/components/NasaForm/NasaForm';
import renderer from 'react-test-renderer';
import { reduxForm } from 'redux-form/immutable';

describe('(Component) NasaForm', () => {
  let component;
  let props;
  let spyOnClick;

  beforeEach(() => {
    props = {
      t() {},
      shouldRenderOldPasswordField: true,
      createNasaItem() {},
      submitting: false,
    };

    spyOnClick = jest.spyOn(props, 'createNasaItem');

    component = mount(initApp(<NasaForm {...props} />));
  });

  it('should render correctly when change password', () => {
    const tree = renderer.create(component).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('should trigger createNasaItem when submitted', () => {
    const btnSend = component.find('form');
    btnSend.simulate('submit');
    expect(spyOnClick).toHaveBeenCalled();
  });
});
