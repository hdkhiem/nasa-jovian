module.exports = {
    'Searching valid': function (browser) {
        browser
            .url('http://localhost:3000')
            .waitForElementVisible('body', 1000)
            .setValue('.AutoSuggestionSearch__container input', 'Chung')
            .assert.elementPresent('.AutoSuggestionSearch__suggestionsList li');
    },
    'Searching invalid': function (browser) {
        browser
            .setValue('.AutoSuggestionSearch__container input', '@DSR%')
            .assert.elementNotPresent('.AutoSuggestionSearch__suggestionsList li')
            .end();
    },
};
