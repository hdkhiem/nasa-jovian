module.exports = {
    'Fill login form': function (browser) {
        browser
            .url('http://localhost:3000/login')
            .waitForElementVisible('body', 1000)
            .setValue('input[name="username"]', 'nightwatch')
            .setValue('input[name="password"]', 'password');
    },

    'Login success': function (browser) {
        browser
            .click('input[type="submit"]')
            .pause(1000)
            .assert.containsText('h4', 'Welcome nightwatch')
            .end();
    },
};
